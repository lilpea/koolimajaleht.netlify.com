---
title: Ajalugu
date: 2019-03-08
---

Liivalaia tänaval on olnud juba ammusest ajast kool. Esimeseks kooliks oli 11. Algkool aga kooli 400 õpilase jaoks hakkas see väikseks jääma. Selle aja direktor plaanis ehitada veel ühte kooli, kuhu mahuks kõik ära. Koolimaja ehitamis kohaks valiti segusõlme maa-ala Kingissepa tänaval ehk praeguse kooli asukoht.

Uut koolimaja pandi ehitama vangid. Ehitustöö oli lohakas, töökorraldus halb nagu tollal tavaline. Kool valmis 1966 aastal. Algselt sai kooli nimeks 47. Keskkool. Hiljem 1991 muudeti kooli nimi Liivalaia keskkooliks, millal sai kool ka oma lipu. Kool sai Liivalaia gümnaasiumiks 1996 aastal.  Tänapäeva kooli nimi on Tallinna Südalinna Kool.

_Info on võetud [kooli kodulehelt](https://www.tallinn.ee/est/sydalinna/Ajalugu-enne-Sudalinna-Kooli)._
