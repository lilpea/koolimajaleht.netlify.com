---
title: Majajuht
date: 2019-04-05
---

Koolil on suur hoov, kus saab palli mängida ja jõulinnakus turnida. Koolihoov koosneb staadionist, kaugushüpperajast, kuulitõukekolmnurgast, betoonplatsist, jõulinnakust, jooksurajast ja tõukerattakuurist. Talvel on õpilastele kättesaadavad kelgud ja lumelabidad.

Meie koolil on keeruka ehitusega söökla, toitu saab ise võtta ja istekohti jääb ülegi. Kõik õpilased peavad piiksutama mingit kaarti, et teenida õigust söögikorrale. Üle ühe magustoidu ei tohi võtta, aga pärast, kui magustoitu üle jääb, võib palju süüa

Peale staadioni on meie koolil ka võimla. See on väga libeda põrandaga ja seda saab suure kummikardinaga keskelt pooleks jagada. Võimlas saab mängida korvapalli, saalihokit ja isegi sulgpalli.

III korrusel asub tulevikuklass, kus saab kasutada laptope ja värvilisi ratastega toole. Seda klassi valgustavad sinakad lambid. Noorematel avaneb seal võimalus programmeerida mesilasroboteid.

Arvutiklass asub II korrusel ja selles leidub erilisi arvutilaudu ja suur hulk raale. 3.-5. klassi õpilased saavad osaleda robootika huviringis.

Keemia klassi muudab eriliseks suur arv kraanikausse ja kahtlane tagaruum, kuhu ühtegi õpilast ei lubata.

Tööõpetuse klassis on metallist tööriistakapid, palju erinevaid puutöömasinaid jne.
