+++
title = "Home"
+++
# TSK koolimaja leht
Tallinna Südalinna Kool on väike kollane kool Tallinna südames. Meie kooli vahetunnid on väga muljetavaldavad. Meil on tantsu-, õue- ja spordivahetunnid. [Vaata kooli kodulehte](https://www.tallinn.ee/sydalinna/)
